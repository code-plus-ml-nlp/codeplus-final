server {
    server_name codeplus-nlp.oit.duke.edu;
    rewrite ^/$ https://codeplus-nlp.oit.duke.edu/voila/render/home_page.ipynb? permanent;
    proxy_buffering off;
    location / {
            proxy_pass http://localhost:8999;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_read_timeout 86400;
## adding auth from 
            auth_basic "Input password to access SciVerify";
            auth_basic_user_file /etc/nginx/.htpasswd;



    }

    client_max_body_size 100M;
    error_log /var/log/nginx/error.log;

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/codeplus-nlp.oit.duke.edu/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/codeplus-nlp.oit.duke.edu/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = codeplus-nlp.oit.duke.edu) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    server_name codeplus-nlp.oit.duke.edu;
    return 404; # managed by Certbot


}

